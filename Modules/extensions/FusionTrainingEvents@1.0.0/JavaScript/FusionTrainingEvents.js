/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module FusionTrainingEvents
define(
	'FusionTrainingEvents'
,	[
		'FusionTrainingEvents.Router'
	]
,	function (
		FusionTrainingEventsRouter
	)
{
	'use strict';

	//@class FusionTrainingEvents @extends ApplicationModule
	return {
		mountToApp: function (application)
		{
			return new FusionTrainingEventsRouter(application);
		}
	};

	if (typeof define === 'function' && define.amd) {
		define('FusionTrainingEvents', [], function() {
			return 'FusionTrainingEvents';
		});
	};
});
