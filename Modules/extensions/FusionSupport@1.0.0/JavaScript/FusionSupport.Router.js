/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module FusionSupport
define(
	'FusionSupport.Router'
,	[
		'FusionSupport.View'

	,	'Backbone'
	]
,	function (
		FusionSupportView

	,	Backbone
	)
{
	'use strict';

	// @lass FusionSupport.Router @extends Backbone.Router
	return Backbone.Router.extend({

		routes: {
			'support': 'page'
		}

	,	initialize: function (Application)
		{
			this.application = Application;
		}

		// @method lookbookPage dispatch the 'go to lookbook page' route
	,	page: function ()
		{
			var view = new FusionSupportView({ application: this.application });

			view.showContent();
		}
	});
});
