{{!

<!--<div class="page-header custom-page-header contact-us-header">
<div class="container">
        <div class="middle-align">
        </div>
    </div>
</div>-->

    /*.map-container {
        height: 380px;
        overflow: hidden;
    }
    #map {
        width: 2304px;
        height: 410px;
    }*/

}}

    <style type="text/css">
        .map-container {
            position: relative;
            height: 380px;
            overflow: hidden;
        }
        #map {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 405px;
        }
    </style>
    <div class="map-container"><div id="map"></div></div>
    <!-- AIzaSyAFkFCqCFjFaizCfsIxvlTXFJ_OqebJbKw -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFkFCqCFjFaizCfsIxvlTXFJ_OqebJbKw&v=3.exp&sensor=false&callback=init"></script>
    <script>
        //google.maps.event.addDomListener(window, 'load', init);

        var map;

        function init() {
            var coords = new google.maps.LatLng(-36.9921581, 174.8854796);
            // TODO coords are off by a bit, wait for it to refresh
            var markerCoords = new google.maps.LatLng(-36.9912671, 174.8865956);

            var mapOptions = {
                    zoom: 16
                ,	center: coords
                ,	mapTypeId: google.maps.MapTypeId.ROADMAP
                ,   disableDefaultUI: true
                ,	styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#3e606f"},{"weight":2},{"gamma":0.84}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"weight":0.6},{"color":"#1a3541"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#2c5a71"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#2c5a71"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#29768a"},{"lightness":-37}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#193341"}]}]
            }

            var mapElement = document.getElementById('map');
            var map = new google.maps.Map(mapElement, mapOptions);

            var arrow = {
                path: 'M 0,0 100,50 50,100 z',
                fillOpacity: 0.2,
                scale: 1,
                strokeColor: 'white',
                strokeWeight: 3
            };

            var marker = new google.maps.Marker({
                position: markerCoords
            ,   icon: arrow
            ,   draggable: true
            ,   map: map
            });
        }
    </script>

<div class="container custom-page contact-us">
    <div class="row">
        <h1>Locations</h1>
        <div class="row locations">
            <div class="col-md-3">
                <div data-cms-area="contact_us_locations_one" data-cms-area-filters="path"></div>
            </div>
            <div class="col-md-3">
                <div data-cms-area="contact_us_locations_two" data-cms-area-filters="path"></div>
            </div>
            <div class="col-md-3">
                <div data-cms-area="contact_us_locations_three" data-cms-area-filters="path"></div>
            </div>
        </div>
    </div>
    <div class="row custom-page-seperator">&nbsp;</div>
    <div class="row contact-us-information">
        <div data-cms-area="contact_us_email" data-cms-area-filters="path"></div>
    </div>
</div>
<div class="shaded-background">
    <div class="container custom-page">
        <div class="the-channel-ten-team">
            <div class="row">
                <div data-cms-area="contact_us_content" data-cms-area-filters="path"></div>
            </div>
            <div class="row individual-profiles">
                <div class="col-md-3">
                    <div data-cms-area="contact_us_contact_the_team_profile_image_1" data-cms-area-filters="path"></div>
                    <div data-cms-area="contact_us_contact_the_team_profile_content_1" data-cms-area-filters="path"></div>
                </div>
                <div class="col-md-3">
                    <div data-cms-area="contact_us_contact_the_team_profile_image_2" data-cms-area-filters="path"></div>
                    <div data-cms-area="contact_us_contact_the_team_profile_content_2" data-cms-area-filters="path"></div>
                </div>
                <div class="col-md-3">
                    <div data-cms-area="contact_us_contact_the_team_profile_image_3" data-cms-area-filters="path"></div>
                    <div data-cms-area="contact_us_contact_the_team_profile_content_3" data-cms-area-filters="path"></div>
                </div>
                <div class="col-md-3">
                    <div data-cms-area="contact_us_contact_the_team_profile_image_4" data-cms-area-filters="path"></div>
                    <div data-cms-area="contact_us_contact_the_team_profile_content_4" data-cms-area-filters="path"></div>
                </div>
            </div>
            <div class="row individual-profiles">
                <div class="col-md-3">
                    <div data-cms-area="contact_us_contact_the_team_profile_image_5" data-cms-area-filters="path"></div>
                    <div data-cms-area="contact_us_contact_the_team_profile_content_5" data-cms-area-filters="path"></div>
                </div>
                <div class="col-md-3">
                    <div data-cms-area="contact_us_contact_the_team_profile_image_6" data-cms-area-filters="path"></div>
                    <div data-cms-area="contact_us_contact_the_team_profile_content_6" data-cms-area-filters="path"></div>
                </div>
                <div class="col-md-3">
                    <div data-cms-area="contact_us_contact_the_team_profile_image_7" data-cms-area-filters="path"></div>
                    <div data-cms-area="contact_us_contact_the_team_profile_content_7" data-cms-area-filters="path"></div>
                </div>
                <div class="col-md-3">
                    <div data-cms-area="contact_us_contact_the_team_profile_image_8" data-cms-area-filters="path"></div>
                    <div data-cms-area="contact_us_contact_the_team_profile_content_8" data-cms-area-filters="path"></div>
                </div>
            </div>
        </div>
    </div>
</div>
