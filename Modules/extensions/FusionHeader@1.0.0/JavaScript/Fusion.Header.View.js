/**
 * hide header-site-search by default same way as Fusion.Home.View.js
 */

/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module FusionHeader
define(
	'Fusion.Header.View'
,	[
		'Backbone'
	,	'Backbone.CompositeView'

	,	'jQuery'
	,	'underscore'
	,	'Utils'

	,	'Header.View'
	]
,	function (
		Backbone
	,	BackboneCompositeView

	,	jQuery
	,	underscore
	,	Utils

    ,   HeaderView
	)
{
	'use strict';

	//@module Fusion.Home.View @extends Backbone.View
	return {

		mountToApp: function (application)
		{
			var self = this;

            // Hide the site search by default on non-desktop devices
			jQuery(document).ready(function () {
				if (Utils.isDesktopDevice() == false) {
                    jQuery('[data-type="SiteSearch"]').hide();
                }
			});
        }
	}
});
