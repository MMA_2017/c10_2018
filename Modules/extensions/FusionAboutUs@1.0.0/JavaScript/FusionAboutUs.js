/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module FusionAboutUs
define(
	'FusionAboutUs'
,	[
		'FusionAboutUs.Router'
	]
,	function (
		FusionAboutUsRouter
	)
{
	'use strict';

	//@class FusionAboutUs @extends ApplicationModule
	return {
		mountToApp: function (application)
		{
			return new FusionAboutUsRouter(application);
		}
	};

	if (typeof define === 'function' && define.amd) {
		define('FusionAboutUs', [], function() {
			return 'FusionAboutUs';
		});
	};
});
