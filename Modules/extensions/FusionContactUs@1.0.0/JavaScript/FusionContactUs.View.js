/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module FusionContactUs
define(
	'FusionContactUs.View'
,	[
		'SC.Configuration'
	,	'Utilities.ResizeImage'

	,	'contact_us.tpl'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration

	,	resizeImage
	,	contact_us_tpl

	,	Backbone
	,	jQuery
	,	_
	,	Utils
	)
{
	'use strict';

	//@module FusionContactUs.View @extends Backbone.View
	return Backbone.View.extend({

		template: contact_us_tpl

	,	title: _('Channel Ten - Contact Us').translate()

	,	page_header: _('Contact Us').translate()

	,	initialize: function (options)
		{
			var self = this;
		}

		// @method getContext @return FusionContactUs.View.Context
	,	getContext: function()
		{
			return {
			};
		}
	});
});
