{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<!-- view to be added to Facets.FacetedNavigation.View.js childViews -->

{{#if showHeading}}
	{{#if isUncollapsible}}
		<div class="facets-faceted-navigation-item-facet-group-expander">
			<h4 class="facets-faceted-navigation-item-facet-group-title">
				{{facetDisplayName}}
				{{#if showRemoveLink}}
				<a class="facets-faceted-navigation-item-filter-delete" href="{{removeLink}}">
					<i class="facets-faceted-navigation-item-filter-delete-icon"></i>
				</a>
				{{/if}}
			</h4>
		</div>
	{{else}}
		<a href="#" class="facets-faceted-navigation-item-facet-group-expander" data-toggle="collapse" data-target="#{{htmlId}} .facets-faceted-navigation-item-facet-group-wrapper" data-type="collapse" title="{{facetDisplayName}}">
			<i class="facets-faceted-navigation-item-facet-group-expander-icon"></i>
			<h4 class="facets-faceted-navigation-item-facet-group-title">{{facetDisplayName}}</h4>
			{{#if showRemoveLink}}
				<a class="facets-faceted-navigation-item-filter-delete" href="{{removeLink}}">
					<i class="facets-faceted-navigation-item-filter-delete-icon"></i>
				</a>
			{{/if}}
		</a>
	{{/if}}
{{/if}}
