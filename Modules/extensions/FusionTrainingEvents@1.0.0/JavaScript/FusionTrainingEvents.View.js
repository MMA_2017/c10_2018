/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module FusionTrainingEvents
define(
	'FusionTrainingEvents.View'
,	[
		'SC.Configuration'

	,	'training_events.tpl'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration

	,	training_events_tpl

	,	Backbone
	,	jQuery
	,	_
	,	Utils
	)
{
	'use strict';

	//@module FusionTrainingEvents.View @extends Backbone.View
	return Backbone.View.extend({

		template: training_events_tpl

	,	title: _('Channel Ten - Training & Events').translate()

	,	page_header: _('Training & Events').translate()

	,	initialize: function (options)
		{
			var self = this;
		}

		// @method getContext @return FusionTrainingEvents.View.Context
	,	getContext: function()
		{
			return {
			};
		}
	});
});
