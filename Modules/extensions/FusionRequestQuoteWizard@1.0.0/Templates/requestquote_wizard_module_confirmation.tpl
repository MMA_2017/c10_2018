{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="requestquote-wizard-module-confirmation">
	<h2 class="requestquote-wizard-module-confirmation-title">{{translate 'Thank you!'}}</h2>
	<p class="requestquote-wizard-module-confirmation-body">
		{{translate 'Your Quote Request <a href="/quotes/$(0)">#$(1)</a> was successfully placed.' quoteId quoteTranId}}
	</p>
	<p class="requestquote-wizard-module-confirmation-body">
		A sales representative will contact be in contact shortly. For immediate assistance call us at <strong>(09) 973 0840</strong> or email us as <a href="mailto:sales@channelten.co.nz">sales@channelten.co.nz</a>.
	</p>
	<a class="requestquote-wizard-module-confirmation-new-quote" href="/request-a-quote">{{translate 'Request a new Quote'}}</a>
	<a class="requestquote-wizard-module-confirmation-continue" href="/quotes">{{translate 'See Your Quotes'}}</a>
</div>
