/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module FusionHome
define(
	'Fusion.Home.View'
,	[
		'Backbone'
	,	'Backbone.CompositeView'

	,	'jQuery'
	,	'underscore'
	,	'Utils'

	,	'Home.View'
	]
,	function (
		Backbone
	,	BackboneCompositeView

	,	jQuery
	,	underscore
	,	Utils

    ,   HomeView
	)
{
	'use strict';

	//@module Fusion.Home.View @extends Backbone.View
	return {

		mountToApp: function (application)
		{
			var self = this;

			HomeView.prototype.title = 'Channel Ten - Security Imports Limited';

			HomeView.prototype.page_header = 'Channel Ten - Security Imports Limited';

			HomeView.prototype.resizeCarousel = function ()
			{
				jQuery(document).ready(function () {
					// Carousel resizing
					jQuery('.home-image-slider-list li').css('width', jQuery('.bootstrap-container').width() + 'px');

					if (Utils.isPhoneDevice() == true) {
						console.log('bx wrapper height');
						//jQuery('.bx-wrapper').css('height', '390px !important');
						//jQuery('.bx-wrapper').css('height', null);
					}

					if (Utils.isTabletDevice() == true) {
						jQuery('.home-slide-caption').css('height', '320px');
					}
				});
			}

			HomeView.prototype.on('afterViewRender', function () {
				//console.log('view rendered');
				/*jQuery('[data-slider]').bxSlider({
					auto: true
				});*/
				/*HomeView.prototype.slider.reloadSlider({
					auto: true
				});*/
			});

			jQuery(document).ready(function () {

				HomeView.prototype.resizeCarousel();
			});

			window.onresize = function () {
				console.log('resized');
				HomeView.prototype.resizeCarousel();
			}

			// Check if the route has changed, if so execute HomeView.prototype.resizeCarousel again
			Backbone.history.on("all", function (route, router) {
				HomeView.prototype.resizeCarousel();
			});
        }
	}
});
