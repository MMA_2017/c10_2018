/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module FusionAboutUs
define(
	'FusionAboutUs.View'
,	[
		'SC.Configuration'
	,	'Utilities.ResizeImage'

	,	'about_us.tpl'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration

	,	resizeImage
	,	about_us_tpl

	,	Backbone
	,	jQuery
	,	_
	,	Utils
	)
{
	'use strict';

	//@module FusionAboutUs.View @extends Backbone.View
	return Backbone.View.extend({

		template: about_us_tpl

	,	title: _('Channel Ten - About Us').translate()

	,	page_header: _('About Us').translate()

	,	initialize: function (options)
		{
			var self = this;
		}

		// @method getContext @return FusionAboutUs.View.Context
	,	getContext: function()
		{
			return {
			};
		}
	});
});
