<div class="page-header custom-page-header about-us-header">
    <div class="container">
        <div class="middle-align">
            <div data-cms-area="about_us_header_content" data-cms-area-filters="path"></div>
        </div>
    </div>
</div>

<div class="container custom-page about-us">
    <div class="row introduction">
        <div data-cms-area="about_us_introduction" data-cms-area-filters="path"></div>
    </div>
</div>
<div class="shaded-background">
    <div class="container custom-page the-channel-ten-team">
        <div class="row">
            <div data-cms-area="about_us_contact_the_team" data-cms-area-filters="path"></div>
        </div>
        <div class="row individual-profiles">
            <div class="col-md-3">
                <div data-cms-area="about_us_contact_the_team_profile_image_1" data-cms-area-filters="path"></div>
                <div data-cms-area="about_us_contact_the_team_profile_content_1" data-cms-area-filters="path"></div>
            </div>
            <div class="col-md-3">
                <div data-cms-area="about_us_contact_the_team_profile_image_2" data-cms-area-filters="path"></div>
                <div data-cms-area="about_us_contact_the_team_profile_content_2" data-cms-area-filters="path"></div>
            </div>
            <div class="col-md-3">
                <div data-cms-area="about_us_contact_the_team_profile_image_3" data-cms-area-filters="path"></div>
                <div data-cms-area="about_us_contact_the_team_profile_content_3" data-cms-area-filters="path"></div>
            </div>
            <div class="col-md-3">
                <div data-cms-area="about_us_contact_the_team_profile_image_4" data-cms-area-filters="path"></div>
                <div data-cms-area="about_us_contact_the_team_profile_content_4" data-cms-area-filters="path"></div>
            </div>
        </div>
        <div class="row individual-profiles">
            <div class="col-md-3">
                <div data-cms-area="about_us_contact_the_team_profile_image_5" data-cms-area-filters="path"></div>
                <div data-cms-area="about_us_contact_the_team_profile_content_5" data-cms-area-filters="path"></div>
            </div>
            <div class="col-md-3">
                <div data-cms-area="about_us_contact_the_team_profile_image_6" data-cms-area-filters="path"></div>
                <div data-cms-area="about_us_contact_the_team_profile_content_6" data-cms-area-filters="path"></div>
            </div>
            <div class="col-md-3">
                <div data-cms-area="about_us_contact_the_team_profile_image_7" data-cms-area-filters="path"></div>
                <div data-cms-area="about_us_contact_the_team_profile_content_7" data-cms-area-filters="path"></div>
            </div>
            <div class="col-md-3">
                <div data-cms-area="about_us_contact_the_team_profile_image_8" data-cms-area-filters="path"></div>
                <div data-cms-area="about_us_contact_the_team_profile_content_8" data-cms-area-filters="path"></div>
            </div>
        </div>
    </div>
</div>
