/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module FusionFacets
define(
	'Fusion.Facets.ResetFilters.View'
,	[
		'facets_faceted_reset_filters.tpl'

	,	'Backbone'
	,	'jQuery'

    ,   'Facets.FacetedNavigationItem.View'
	]
,	function (
		facets_faceted_reset_filters_tpl

	,	Backbone
	,	jQuery

    ,   FacetsFactedNavigationItemView
	)
{
	'use strict';

	//@module Fusion. @extends Backbone.View
	return Backbone.View.extend({

		template: facets_faceted_reset_filters_tpl

    ,   initialize: function ()
        {
            console.log('reset filters view');
        }

		// @method getContext @return FusionHeaderSocialMediaView.View.Context
	,	getContext: function()
		{
			return {

			};
		}
	});
});
