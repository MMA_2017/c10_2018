/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module FusionResellers
define(
	'FusionResellers.Router'
,	[
		'FusionResellers.View'

	,	'Backbone'
	]
,	function (
		FusionResellersView

	,	Backbone
	)
{
	'use strict';

	// @lass FusionResellers.Router @extends Backbone.Router
	return Backbone.Router.extend({

		routes: {
			'resellers': 'page'
		}

	,	initialize: function (Application)
		{
			this.application = Application;
		}

		// @method lookbookPage dispatch the 'go to lookbook page' route
	,	page: function ()
		{
			var view = new FusionResellersView({ application: this.application });

			view.showContent();
		}
	});
});
