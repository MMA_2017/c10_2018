{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="facets-categories">
	{{#each categories}}
		{{#if categories}}
		<ul class="facets-categories-list">
			<li>
				<ul class="facets-categories-children">
					{{#each categories}}
					<li>
						<a class="facets-faceted-navigation-item-facet-option {{#if isActive}}option-active{{/if}}" {{objectToAtrributes this}} data-toggle="collapse" data-target=".{{dropdownName text}}" data-type="collapse">
								{{#if this.isActive}}
									<img src="/c.3471189/sca-dev-vinson/img/assets/facets-navigation-active.png" class="facets-faceted-navigation-option-active" />
								{{else}}
									<img src="/c.3471189/sca-dev-vinson/img/assets/facets-navigation-inactive.png" class="facets-faceted-navigation-option-active" />
								{{/if}}
							<span class="facets-facted-navigation-option-displayname">
								{{translate text}}
							</span>

							<span class="facets-facted-navigation-icon-dropdown">
								&gt;
							</span>
						</a>
						<div class="{{dropdownName text}} collapse {{dropdownCurrent text}} {{#if this.isActive}}in{{/if}}">
                        {{#if categories}}
	                        <ul class="facets-categories-siblings">
	                            {{#each categories}}
	                                <li>
	                                    <a class="facets-faceted-navigation-item-facet-option {{#if isActive}}option-active{{/if}}" {{objectToAtrributes this}}>
	                                            {{#if this.isActive}}
	                                                <img src="/c.3471189/sca-dev-vinson/img/assets/facets-navigation-active.png" class="facets-faceted-navigation-option-active" />
	                                            {{else}}
	                                                <img src="/c.3471189/sca-dev-vinson/img/assets/facets-navigation-inactive.png" class="facets-faceted-navigation-option-active" />
	                                            {{/if}}
	                                        <span class="facets-facted-navigation-option-displayname">
	                                            {{translate text}}
	                                        </span>
	                                    </a>
	                                </li>
	                            {{/each}}
	                        </ul>
                        {{/if}}
						</div>
					</li>
					{{/each}}
				</ul>
			</li>
		</ul>
		{{/if}}
	{{/each}}
</div>
