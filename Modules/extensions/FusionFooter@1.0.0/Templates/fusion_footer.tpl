{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="footer-content">

	<div id="banner-footer" class="content-banner banner-footer" data-cms-area="global_banner_footer" data-cms-area-filters="global"></div>

	<div class="footer-content-nav">
		{{#if showFooterNavigationLinks}}
			<!-- TODO navigation needs to be made into rows, <div class="footer-content-nav-list"> ul li </div> per row -->
			<ul class="footer-content-nav-list">
				{{#each footerNavigationLinks}}
					<li>
						<a {{objectToAtrributes item}}>
							{{text}}
						</a>
					</li>
				{{/each}}
			</ul>
		{{/if}}
	</div>

	<div class="footer-content-right">
		<div data-view="FooterContent"></div>
	</div>

	<div class="container">
		<div class="row footer-bottom">
			<div class="footer-content-social">
				<!-- social media links are defined in Configuration/Footer.json -->
				<ul>
					<li><a href="https://www.facebook.com/channeltennz/"><img src="/c.3471189/sca-dev-vinson/img/facebook@2x.png" alt="Channel Ten on Facebook" title="Channel Ten on Facebook" /></a></li>
					<li><a href="https://www.linkedin.com/company/channel-ten-security-imports-ltd"><img src="/c.3471189/sca-dev-vinson/img/linkedin@2x.png" alt="Channel Ten on LinkedIn" title="Channel Ten on LinkedIn" /></a></li>
					<li><a href="https://www.youtube.com/channel/UCoTInK1Qrsd7huQaL2sCRJQ"><img src="/c.3471189/sca-dev-vinson/img/yt-200px.png" alt="Channel Ten on YouTube" title="Channel Ten on YouTube" /></a></li>
					<li><a href="https://www.instagram.com/channeltennz/"><img src="/c.3471189/sca-dev-vinson/img/ig-200px.png" alt="Channel Ten on Instagram" title="Channel Ten on Instagram" /></a></li>
				</ul>
			</div>
			<div class="footer-content-copyright">
				{{!
					<p>&copy; Copyright 2017 <a href="/" data-touchpoint="home" data-hashtag="#/" class="company-name">Channel Ten Security Imports Limited</a></p>
				}}
				<p>&copy; Copyright 2017 <span>Channel Ten Security Imports Limited</span></p>
			</div>
		</div>
	</div>
</div>
