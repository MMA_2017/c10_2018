<div class="page-header custom-page-header support-header">
    <div class="container">
        <div class="middle-align">
            <div class="support-container">
                <div data-cms-area="support_header_content" data-cms-area-filters="path"></div>
                <!--<button class="btn-primary get-support-button"><a href="#newcase" data-touchpoint="customercenter" data-hashtag="#newcase">Get Support</a></button>-->
            </div>
        </div>
    </div>
</div>

<div class="container custom-page support">
    <div class="row support-content">
        <div data-cms-area="support_content" data-cms-area-filters="path"></div>
        <p><button class="btn-primary get-support-button"><a href="#newcase" data-touchpoint="customercenter" data-hashtag="#newcase">Get Support</a></button></p>
    </div>
</div>
