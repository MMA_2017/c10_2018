{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="home">
	<div class="home-slider-container">
		<div class="home-image-slider">
			<ul data-slider class="home-image-slider-list">
				{{#each carouselImages}}
					<li style="background: url({{resizeImage this ../imageHomeSize}});">
						<div class="channel-ten-carousel-slide">
							<!--<img src="{{resizeImage this ../imageHomeSize}}" alt="" />-->
							<!--<div class="home-slide-caption font-circular">
								<h1 class="home-slide-caption-title">Circular</h1>-->
								<!--<h2 class="home-slide-caption-title">Calibri</h2>
								<h3 class="home-slide-caption-title">Source Sans Pro</h3>-->
								<!--<p>Example descriptive text displayed on multiple lines.</p>
								<div class="home-slide-caption-button-container">
									<a href="/search" class="home-slide-caption-button">Shop Now</a>
								</div>
							</div>-->
						</div>
					</li>
				{{/each}}
			</ul>
		</div>
	</div>

	{{#if bottomBannerImages}}
	<div class="call-to-actions container">
		<div class="row">
			{{#each bottomBannerImages}}
			<div class="col-md-4 center action-shortcut">
				<img src="{{resizeImage this ../imageHomeSizeBottom}}" alt="{{this.text}}" />
				<p>Action text caption <span class="">&gt;</span></p>
			</div>
			{{/each}}
		</div>
	</div>
	{{/if}}
</div>

<script language="javascript">

jQuery(document).ready(function () {

	var left = Number(0);

	jQuery('.action-shortcut p').each(function (index) {
		if (jQuery('.action-shortcut img:nth-child(' + index + ')').offset() !== undefined) {
			left = jQuery('.action-shortcut img:nth-child(' + index + ')').offset();
	    }
		console.log('left position: ' + JSON.stringify(left) + ' index: ' + index);
		//jQuery('.action-shortcut p:nth-child(' + index + ')').css('left', left);
	});

	jQuery('.action-shortcut img').each(function (i) {
		jQuery('.action-shortcut p:nth-child(' + i + ')').css('position', 'absolute');
		jQuery('.action-shortcut p:nth-child(' + i + ')').css('left', left + 'px');
		jQuery('.action-shortcut p:nth-child(' + i + ')').css('width', '150px');
	});
});

</script>
