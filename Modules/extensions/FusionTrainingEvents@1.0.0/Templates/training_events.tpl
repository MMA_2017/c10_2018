<div class="page-header custom-page-header training-and-events-header">
    <div class="container">
        <div class="middle-align">
            <div data-cms-area="training_and_events_header_content" data-cms-area-filters="path"></div>
        </div>
    </div>
</div>

<div class="container custom-page training-and-events">
    <div class="row introduction">
        <!--<p>Copy about how Milestone and Axis offer training offer</p>
        <p>how c10 does other things</p>-->
        <div data-cms-area="training_and_events_introduction" data-cms-area-filters="path"></div>
    </div>
    <div class="row training-and-events-logos">
        <div class="col-md-6 milestone-logo">
            <img src="/sca-dev-vinson/img/logos/milestone%20logo@2x.png" alt="Milestone" title="Milestone" />
        </div>
        <div class="col-md-6 axis-communications-logo">
            <img src="/sca-dev-vinson/img/logos/axis_logo@2x.png" alt="Axis Communications" title="Axis Communications" />
        </div>
    </div>
    <div class="row information">
        <div data-cms-area="training_and_events_information" data-cms-area-filters="path"></div>
        <!--<p>Copy about contacting James and the role that he</p>
        <p>fulfills when it comes to training</p>-->
    </div>
</div>

<div class="shaded-background">
    <div class="container custom-page">
        <div class="the-channel-ten-team">
            <div class="row individual-profiles">
                <div class="col-md-3">
                    <div data-cms-area="training_and_events_image_1" data-cms-area-filters="path"></div>
                    <div data-cms-area="training_and_events_content_1" data-cms-area-filters="path"></div>
                </div>
                <div class="col-md-3">
                    <div data-cms-area="training_and_events_image_2" data-cms-area-filters="path"></div>
                    <div data-cms-area="training_and_events_content_2" data-cms-area-filters="path"></div>
                </div>
                <div class="col-md-3">
                    <div data-cms-area="training_and_events_image_3" data-cms-area-filters="path"></div>
                    <div data-cms-area="training_and_events_content_3" data-cms-area-filters="path"></div>
                </div>
                <div class="col-md-3">
                    <div data-cms-area="training_and_events_image_4" data-cms-area-filters="path"></div>
                    <div data-cms-area="training_and_events_content_4" data-cms-area-filters="path"></div>
                </div>
            </div>
            <div class="row individual-profiles">
                <div class="col-md-3">
                    <div data-cms-area="training_and_events_profile_image_5" data-cms-area-filters="path"></div>
                    <div data-cms-area="training_and_events_profile_content_5" data-cms-area-filters="path"></div>
                </div>
                <div class="col-md-3">
                    <div data-cms-area="training_and_events_profile_image_6" data-cms-area-filters="path"></div>
                    <div data-cms-area="training_and_events_profile_content_6" data-cms-area-filters="path"></div>
                </div>
                <div class="col-md-3">
                    <div data-cms-area="training_and_events_profile_image_7" data-cms-area-filters="path"></div>
                    <div data-cms-area="training_and_events_profile_content_7" data-cms-area-filters="path"></div>
                </div>
                <div class="col-md-3">
                    <div data-cms-area="training_and_events_profile_image_8" data-cms-area-filters="path"></div>
                    <div data-cms-area="training_and_events_profile_content_8" data-cms-area-filters="path"></div>
                </div>
            </div>
        </div>
    </div>
</div>
