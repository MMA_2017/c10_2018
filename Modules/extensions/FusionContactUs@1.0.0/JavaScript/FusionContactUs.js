/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module FusionContactUs
define(
	'FusionContactUs'
,	[
		'FusionContactUs.Router'
	]
,	function (
		FusionContactUsRouter
	)
{
	'use strict';

	//@class FusionContactUs @extends ApplicationModule
	return {
		mountToApp: function (application)
		{
			return new FusionContactUsRouter(application);
		}
	};

	if (typeof define === 'function' && define.amd) {
		define('FusionContactUs', [], function() {
			return 'FusionContactUs';
		});
	};
});
