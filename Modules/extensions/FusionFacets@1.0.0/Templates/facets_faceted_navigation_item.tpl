{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{#if showFacet}}
	<div class="facets-faceted-navigation-item-facet-group" id="{{htmlId}}" data-type="rendered-facet" data-facet-id="{{facetId}}">
		<div class="collapse in facets-faceted-navigation-item-facet-group-wrapper">
			<div class="facets-faceted-navigation-item-facet-group-content">
				<ul class="facets-faceted-navigation-item-facet-optionlist">
					{{#each displayValues}}
						<li>
							<a class="facets-faceted-navigation-item-facet-option {{#if isActive}}option-active{{/if}}" href="{{link}}" title="{{label}}">
									{{#if isActive}}
										<img src="/c.3471189/sca-dev-vinson/img/assets/facets-navigation-active.png" class="facets-faceted-navigation-option-active" />
									{{else}}
										<img src="/c.3471189/sca-dev-vinson/img/assets/facets-navigation-inactive.png" class="facets-faceted-navigation-option-active" />
									{{/if}}
								<span class="facets-facted-navigation-option-displayname">
									{{displayName}}
								</span>

								<span class="facets-facted-navigation-icon-dropdown">
									&gt;
								</span>
							</a>
						</li>
					{{/each}}
					{{#each extraValues}}
						<li>
							<a class="facets-faceted-navigation-item-facet-option {{#if isActive}}option-active{{/if}}" href="{{link}}" title="{{label}}">
									{{#if isActive}}
										<img src="/c.3471189/sca-dev-vinson/img/assets/facets-navigation-active.png" class="facets-faceted-navigation-option-active" />
									{{else}}
										<img src="/c.3471189/sca-dev-vinson/img/assets/facets-navigation-inactive.png" class="facets-faceted-navigation-option-active" />
									{{/if}}
								<span class="facets-facted-navigation-option-displayname">
									{{displayName}}
								</span>

								<span class="facets-facted-navigation-icon-dropdown">
									&gt;
								</span>
							</a>
						</li>
					{{/each}}
				</ul>
			</div>
		</div>
	</div>
{{/if}}
