/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module FusionTermsOfTrade
define(
	'FusionTermsOfTrade.View'
,	[
		'SC.Configuration'
	,	'Utilities.ResizeImage'

	,	'terms_of_trade.tpl'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration

	,	resizeImage
	,	terms_of_trade_tpl

	,	Backbone
	,	jQuery
	,	_
	,	Utils
	)
{
	'use strict';

	//@module FusionTermsOfTrade.View @extends Backbone.View
	return Backbone.View.extend({

		template: terms_of_trade_tpl

	,	title: _('Channel Ten - Terms Of Trade').translate()

	,	page_header: _('Terms Of Trade').translate()

	,	initialize: function (options)
		{
			var self = this;
		}

		// @method getContext @return FusionTermsOfTrade.View.Context
	,	getContext: function()
		{
			return {
			};
		}
	});
});
