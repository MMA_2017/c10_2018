/**
 * Adds additional section to the Checkout process (Currently Quotes)
 * In addition, you need to extend the SC.MyAccount.Configuration file in suitecommerce directory
 *
 * @author Gareth Stones <gatty.nz@gmail.com>
 * @version 1.0.0
 * @module FusionOrderWizard.Module.CheckoutFields
 */

define(
	'FusionOrderWizard.Module.CheckoutFields'
,	[
    	'Wizard.Module'

	,	'CheckoutFields.Model'

	,	'order_wizard_checkout_fields_module.tpl'

	,	'underscore'
	,	'Utils'
	]
,	function (
		WizardModule

	,	Model

	,	order_wizard_checkout_fields_module_tpl

	,	_
	)
{
	'use strict';

	return WizardModule.extend({

		template: order_wizard_checkout_fields_module_tpl

		// Events to update the model then submit
	,	events: {
			'click [data-action="submit"]': 'submit'
		}

        /**
         * Render our awesome view
         */

	/*,	render: function ()
		{
			this.application = this.wizard.application;
			this.profile = this.wizard.options.profile;
			this.options.application = this.wizard.application;

			if (this.isActive())
			{
				this._render();
			}
		}*/

        /**
         * Constructor to render our view when various events occur
         *
         * @return {Void}
         */

	,	initialize: function ()
		{
			var self = this;

			console.log('FusionOrderWizard');

			WizardModule.prototype.initialize.apply(this, arguments);

			this.wizard.model.on('ismultishiptoUpdated', function ()
			{
				self.render();
			});

			this.wizard.model.on('promocodeUpdated', function ()
			{
				self.render();
			});

			console.log('checkoutFields model: ' + JSON.stringify(this.model));
			console.log('checkoutFields model: ' + JSON.stringify(this.wizard.model));
		}

		/**
		 * Update the associated model with new value
		 */

	,	submit: function (e)
		{
			var po_number = Number(0)
			,	slc_number = Number(0);

			po_number = jQuery('#po_number').val();
			slc_number = jQuery('#slc_number').val();

			// Get target field name and value
			console.log('CheckoutFields form submitted');

			console.log('PO Number: ' + JSON.stringify(po_number));
			console.log('SLC Number: ' + JSON.stringify(slc_number));

			this.wizard.model.set('otherrefnum', po_number);
			this.wizard.model.set('custbody_slc_number', slc_number);
		}
	});
});
