{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="container bootstrap-container">
	<div class="row">
		<div class="col-sm-4 bootstrap-cell"></div>
	</div>
</div>

<div class="home">
	<div class="home-slider-container">
		<div class="home-image-slider">
			<ul data-slider class="home-image-slider-list">
				<li>
					<div class="home-slide-main-container carousel-axis active">
						<div data-cms-area="homepage_carousel_image_1" data-cms-area-filters="path"></div>
						<div class="home-slide-caption">
							<h2 class="home-slide-caption-title">
								<div data-cms-area="homepage_carousel_heading_1" data-cms-area-filters="path"></div>
							</h2>
							<p>
								<div data-cms-area="homepage_carousel_caption_1" data-cms-area-filters="path"></div>
							</p>
							<!--<div class="home-slide-caption-button-container">
								<button class="home-slide-caption-button"><a href="/vendors/axis/Fixed-Cameras">Shop Now</a></button>
							</div>-->
						</div>
					</div>
				</li>
				<li>
					<div class="home-slide-main-container carousel-milestone">
					<div data-cms-area="homepage_carousel_image_2" data-cms-area-filters="path"></div>
						<div class="home-slide-caption">
							<h2 class="home-slide-caption-title">
								<div data-cms-area="homepage_carousel_heading_2" data-cms-area-filters="path"></div>
							</h2>
							<p>
								<div data-cms-area="homepage_carousel_caption_2" data-cms-area-filters="path"></div>
							</p>
							<!--<div class="home-slide-caption-button-container">
								<button class="home-slide-caption-button"><a href="/vendors/milestone/Husky">Shop Now</a></button>
							</div>-->
						</div>
					</div>
				</li>
				<li>
					<div class="home-slide-main-container carousel-2n">
					<div data-cms-area="homepage_carousel_image_3" data-cms-area-filters="path"></div>
						<div class="home-slide-caption">
							<h2 class="home-slide-caption-title">
								<div data-cms-area="homepage_carousel_heading_3" data-cms-area-filters="path"></div>
							</h2>
							<p>
								<div data-cms-area="homepage_carousel_caption_3" data-cms-area-filters="path"></div>
							</p>
							<!--<div class="home-slide-caption-button-container">
								<button class="home-slide-caption-button"><a href="/vendors/2N">Shop Now</a></button>
							</div>-->
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>

	<div class="home-banner-main">
		<div class="home-banner-main-cell-nth1">
			<div class="home-banner-main-cell-bg">
				<div data-cms-area="homepage_action_image_1" data-cms-area-filters="path" class="actions"></div>
				<div class="home-banner-main-cell-text">
					<div data-cms-area="homepage_action_caption_1" data-cms-area-filters="path"></div>
				</div>
			</div>
		</div>
		<div class="home-banner-main-cell-nth2">
			<div class="home-banner-main-cell-bg">
				<div data-cms-area="homepage_action_image_2" data-cms-area-filters="path" class="actions"></div>
				<div class="home-banner-main-cell-text">
					<div data-cms-area="homepage_action_caption_2" data-cms-area-filters="path"></div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 home-banner-main-cell-nth3">
			<div class="home-banner-main-cell-bg">
				<div data-cms-area="homepage_action_image_4" data-cms-area-filters="path" class="actions"></div>
				<div class="home-banner-main-cell-text">
					<div data-cms-area="homepage_action_caption_4" data-cms-area-filters="path"></div>
					<!--<p><a href="/training-and-events">Tailored training to suit your organisation</a></p>-->
				</div>
			</div>
		</div>
	</div>
</div>
