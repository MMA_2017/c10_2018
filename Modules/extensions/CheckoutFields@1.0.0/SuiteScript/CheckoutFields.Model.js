/**
 * Submit the new data...
 *
 * @author Gareth Stones <gatty.nz@gmail.com>
 */

define('Address.Model'
,	[
		'SC.Model'
	,	'Models.Init'

	,	'Backbone.Validation'
	,	'underscore'
	]
,	function (
		SCModel
	,	ModelsInit

	,	BackboneValidation
	,	_
	)
{
	'use strict';

	return SCModel.extend({

		name: 'CheckoutFields'

    ,   post: function (data)
        {
            return true;
        }
	});
});
