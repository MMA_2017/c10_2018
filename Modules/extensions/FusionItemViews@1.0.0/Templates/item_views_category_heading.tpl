{{!
    represents the heading displayed under the title for items
    otherwise can be done via facets
}}

{{#if showStockDescription}}
    <p class="item-views-stock-msg-description {{stockInfo.stockDescriptionClass}}">
        <i class="item-views-stock-icon-description"></i>
        {{stockInfo.stockDescription}}
    </p>
{{/if}}
