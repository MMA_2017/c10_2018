{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<a class="header-logo" href="{{headerLinkHref}}" data-touchpoint="{{headerLinkTouchPoint}}" data-hashtag="{{headerLinkHashtag}}" title="{{headerLinkTitle}}">
	{{#if logoUrl}}
	<img src="/sca-dev-vinson/img/logos/channel-10-logo@2x.png" class="channel-ten-logo" />
	<!--<svg width="263" height="26">
		<image xlink:href="/sca-dev-vinson/img/logos/channel-10-logo.svg" src="/sca-dev-vinson/img/logos/channel-10-logo@2x.png" width="263" height="26" />
	</svg>-->
	{{/if}}
</a>
