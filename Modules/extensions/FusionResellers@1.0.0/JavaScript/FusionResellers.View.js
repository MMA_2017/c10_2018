/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module FusionResellers
define(
	'FusionResellers.View'
,	[
		'SC.Configuration'

	,	'resellers.tpl'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration

	,	resellers_tpl

	,	Backbone
	,	jQuery
	,	_
	,	Utils
	)
{
	'use strict';

	//@module FusionResellers.View @extends Backbone.View
	return Backbone.View.extend({

		template: resellers_tpl

	,	title: _('Channel Ten - Resellers').translate()

	,	page_header: _('Resellers').translate()

	,	initialize: function (options)
		{
			var self = this;
		}

		// @method getContext @return FusionResellers.View.Context
	,	getContext: function()
		{
			return {
			};
		}
	});
});
