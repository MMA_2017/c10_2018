define(
	'CheckoutFields.ServiceController'
,	[
		'ServiceController'
	,	'Application'
	,	'CheckoutFields.Model'
	]
,	function(
		ServiceController
	,	Application
	,	CheckoutFields
	)
	{
		'use strict';

		// @class Address.ServiceController Manage addresses requests
		// @extend ServiceController
		return ServiceController.extend({

			// @property {String} name Mandatory for all ssp-libraries model
			name:'CheckoutFields.ServiceController'

		,	post: function()
			{
				this.sendContent(CheckoutFields.post(this.data), {'status': 202});
			}
		});
	}
);
