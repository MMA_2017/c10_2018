/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module FusionFacets
define(
	'Fusion.Facets.Navigation.Headings.View'
,	[
		'facets_faceted_navigation_headings.tpl'

	,	'Backbone'
	,	'jQuery'

    ,   'Facets.FacetedNavigationItem.View'
	]
,	function (
		facets_faceted_navigation_headings_tpl

	,	Backbone
	,	jQuery

    ,   FacetsFactedNavigationItemView
	)
{
	'use strict';

	//@module Fusion.Facets.FacetedNavigation.View @extends Backbone.View
	return Backbone.View.extend({

		template: facets_faceted_navigation_headings_tpl

	,	initialize: function (options)
		{
			var self = this;

			console.log('facets_faceted_navigation_headings_tpl');
		}

		// @method getContext @return Fusion.Facets.FacetedNavigation.View.Context
	,	getContext: function()
		{
			console.log('facets_faceted_navigation_headings_tpl');
			
			return {

			};
		}
	});
});
