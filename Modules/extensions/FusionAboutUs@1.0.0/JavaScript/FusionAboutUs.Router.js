/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module FusionAboutUs
define(
	'FusionAboutUs.Router'
,	[
		'FusionAboutUs.View'

	,	'Backbone'
	]
,	function (
		FusionAboutUsView

	,	Backbone
	)
{
	'use strict';

	// @lass FusionAboutUs.Router @extends Backbone.Router
	return Backbone.Router.extend({

		routes: {
			'about-us': 'page'
		}

	,	initialize: function (Application)
		{
			this.application = Application;
		}

		// @method lookbookPage dispatch the 'go to lookbook page' route
	,	page: function ()
		{
			var view = new FusionAboutUsView({ application: this.application });

			view.showContent();
		}
	});
});
