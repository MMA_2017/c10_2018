/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Facets
define('Facets.FacetedNavigationItemCategory.View'
,	[
		'SC.Configuration'

    ,	'facets_faceted_navigation_item_category.tpl'

    ,	'Backbone'
    ,	'Backbone.CompositeView'
    ,	'underscore'
	]
,	function (
		Configuration

    ,	facets_faceted_navigation_item_category_tpl

    ,	Backbone
    ,	BackboneCompositeView
    ,	_
	)
{
    'use strict';

    // @class Facets.FacetedNavigationItemCategory.View @extends Backbone.View
    return Backbone.View.extend({

        template: facets_faceted_navigation_item_category_tpl

	,	events: {
			'click [data-action="see-more"]': 'toggleSeeMoreLess'
		,	'click [data-toggle="collapse"]': 'scrollToPosition'
		}

	,	position: jQuery(window).scrollTop()

    ,	initialize: function ()
		{
			this.categories = this.model && (this.model.get('siblings') || this.model.get('categories')) || [];
			this.categoryUrl = this.options.categoryUrl;

			this.on('afterViewRender', this.renderCategories, this);
        }

	,	scrollToPosition: function (event)
		{
			var target = this.$(event.currentTarget);

			if (jQuery(target.data('target')).hasClass('in')) {
				jQuery(target.data('target')).hide();
			}

			else {
				jQuery(target.data('target')).show();
			}

			jQuery(window).scrollTop(this.position);
		}

		//@method renderCategories
	,	renderCategories: function ()
		{
			this.$('[data-collapsed="true"]').hide();
		}

	,	toggleSeeMoreLess: function(event)
		{
			var target = this.$(event.currentTarget)
			,	target_see_more = target.find('[data-type="see-more"]')
			,	target_see_less = target.find('[data-type="see-less"]')
			,	target_was_expanded = !!target.data('collapsed');

			if (target_was_expanded)
			{
				target_see_more.show();
				target_see_less.hide();
			}
			else
			{
				target_see_more.hide();
				target_see_less.show();
			}

			target.data('collapsed', !target_was_expanded);
		}

        // @method getContext @return {Facets.FacetedNavigationItemCategory.View.Context}
    ,	getContext: function()
		{
			var showFacet = this.categories.length
			,	values = []
			,	self = this
			,	showMax = Configuration.get('categories.sideMenu.showMax')
			,	uncollapsible = Configuration.get('categories.sideMenu.uncollapsible')
			,	collapsed = Configuration.get('categories.sideMenu.collapsed')
			,	breadcrumbName = null
			,	useClearanceURL = false
			,	pathname = (window.location.pathname.indexOf('shopping-local') != -1) ? window.location.hash.replace('#', '/') : window.location.pathname
			,	fullpath = pathname.split('/')
			,	navigationData = null
			// TODO possibly convert this into an array?
			,	isActive = null;

			fullpath.shift();
			fullpath.shift();

			console.log('pathname: ' + pathname);

			_.each(this.categories, function (category)
			{
				values.push({
					displayName: category.name
				,	label: category.name
				,	link: category.fullurl
				,	isActive: category.fullurl === self.categoryUrl
				});
			});

			var max = showMax || values.length
			,	displayValues = _.first(values, max)
			,	extraValues = _.rest(values, max);

			var breadcrumb = this.model && (this.model.get('breadcrumb') || [])
			,	parentName = '';

			if (breadcrumb && breadcrumb.length)
			{
				var index = breadcrumb.length > 1 ? breadcrumb.length - 2 : breadcrumb.length - 1;
				parentName = breadcrumb[index].name;
			}

			try {
				breadcrumbName = breadcrumb[0].name;
			}

			catch (e) {
				breadcrumbName = 'Products';
			}

			if (pathname.indexOf('clearance') > 0) {
				useClearanceURL = true;
			}

			navigationData = _.where(Configuration.navigationData, { "text": breadcrumbName }) || [];

			navigationData[0].categories = _.sortBy(navigationData[0].categories, function (category) {
				return category.text;
			});

			//console.log('navigationData: ' + JSON.stringify(navigationData));

			// First level highlighting
			_.each (navigationData[0].categories, function (value, keys) {
				if (pathname.indexOf(value.href) != -1) {
					value.isActive = true;
					//console.log('setting to active: ' + value.href + ' - pathname: ' + pathname);
			    }
				else {
					//console.log('value inactive: ' + value.href + ' - pathname: ' + pathname);
					// This will make only the currently selected category active
					// Remove this to have multiple dropdowns work
					// TODO functionality
					value.isActive = false;
				}
			});

			// Second level highlighting
			_.each (navigationData[0].categories, function (value, keys) {
				if (pathname.indexOf(value.href) != -1) {
					value.isActive = true;
					//console.log('level 2 highlighting, value.href: ' + value.href + ' - pathname: ' + pathname);
					if (_.isArray(value.categories)) {
						console.log('more categories');
			            _.each (value.categories, function (sibling, index) {
							if (pathname.indexOf(sibling.href) != -1) {
			                	//console.log('sibling information: ' + JSON.stringify(sibling));
								// update value of parent
								console.log('sibling index: ' + index + ' sibling.href: ' + sibling.href);
								sibling.isActive = true;
								// test this line
								navigationData[0].categories[keys].categories[index].isActive = true;
			                }
							else {
								delete navigationData[0].categories[keys].categories[index].isActive;
								console.log('sibling not found');
							}
			            });
					}
			    }
			});

			//console.log('navigationData includes sibling: ' + JSON.stringify(navigationData[0]));

			//console.log('categories list: ' + _.where(navigationData[0].categories, { 'isActive': true }));

			// idea is to get the category name via the fullpath, and use jQuery to set the CSS properties for
			// it to display, such as display: block, height: initial etc
			//console.log('fullpath: ' + JSON.stringify(fullpath));

			// After the view has rendered, let's try the above logic
			this.on('afterViewRender', function () {
				console.log('afterViewRender');
				setTimeout(function () {
					if (fullpath.length > 0) {
						jQuery('.' + fullpath[0]).css({
							'height': 'initial'
						,	'display': 'block'
						})
						if (!jQuery('.' + fullpath[0]).hasClass('in')) {
							jQuery('.' + fullpath[0]).addClass('in');
						}
						console.log('jQuery(".' + fullpath[0] + '").hasClass("in"): ' + jQuery('.' + fullpath[0]).hasClass('in'));
					}
				}, 0);
			});

			//console.log('navigationData: ' + JSON.stringify(navigationData));

            // @class Facets.FacetedNavigationItemCategory.View.Context
            return {
				//@property {String} htmlId
				htmlId: _.uniqueId('commercecategory_')
                // @property {String} facetId
            ,	facetId: 'commercecategory'
                // @property {Boolean} showFacet
            ,	showFacet: !!showFacet
				//@property {Array<Object>} values
			,	values: values
                // @property {Array<Object>} displayValues
            ,	displayValues: displayValues
				//@property {Array<Object>} extraValues
			,	extraValues: extraValues
				//@property {Boolean} showExtraValues
			,	showExtraValues: !!extraValues.length
				//@property {Boolean} isUncollapsible
			,	isUncollapsible: !!uncollapsible
				//@property {Boolean} isCollapsed
			,	isCollapsed: !uncollapsible && collapsed
				//@property {String} parentName
			,	parentName: parentName
			// @class Facets.FacetedNavigationItemCategory.View

				// @property {Array<NavigationData>} navigationItems
			,	categories: _.where(Configuration.navigationData, { "text": breadcrumbName }) || []
				// @property {Boolean} useClearanceURL
			,	useClearanceURL: useClearanceURL

			,	highlightCurrent: pathname
			,	url: pathname
            };
        }
    });
});
