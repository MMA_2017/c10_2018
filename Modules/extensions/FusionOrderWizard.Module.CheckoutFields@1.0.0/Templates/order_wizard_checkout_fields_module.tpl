{{!
    Adds additional fields to any process (Checkout, Quotes etc)
}}

<div class="order-wizard-checkout-fields-module">
    <div class="checkout-fields">
        <p>
            <label for="po_number">PO Number: </label>
            <input type="text" name="po_number" id="po_number" />
        </p>
        <p>
            <label for="slc_number">SLC Number: </label>
            <input type="text" name="slc_number" id="slc_number" />
        </p>
    </div>
</div>
