/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module FusionTermsOfTrade
define(
	'FusionTermsOfTrade'
,	[
		'FusionTermsOfTrade.Router'
	]
,	function (
		FusionTermsOfTradeRouter
	)
{
	'use strict';

	//@class FusionTermsOfTrade @extends ApplicationModule
	return {
		mountToApp: function (application)
		{
			return new FusionTermsOfTradeRouter(application);
		}
	};

	if (typeof define === 'function' && define.amd) {
		define('FusionTermsOfTrade', [], function() {
			return 'FusionTermsOfTrade';
		});
	};
});
