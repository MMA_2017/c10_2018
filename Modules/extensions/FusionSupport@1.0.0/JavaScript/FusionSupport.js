/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module FusionSupport
define(
	'FusionSupport'
,	[
		'FusionSupport.Router'
	]
,	function (
		FusionSupportRouter
	)
{
	'use strict';

	//@class FusionSupport @extends ApplicationModule
	return {
		mountToApp: function (application)
		{
			return new FusionSupportRouter(application);
		}
	};

	if (typeof define === 'function' && define.amd) {
		define('FusionSupport', [], function() {
			return 'FusionSupport';
		});
	};
});
