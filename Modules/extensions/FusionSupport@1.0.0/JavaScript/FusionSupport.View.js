/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module FusionSupport
define(
	'FusionSupport.View'
,	[
		'SC.Configuration'
	,	'Utilities.ResizeImage'

	,	'support.tpl'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration

	,	resizeImage
	,	support_tpl

	,	Backbone
	,	jQuery
	,	_
	,	Utils
	)
{
	'use strict';

	//@module FusionSupport.View @extends Backbone.View
	return Backbone.View.extend({

		template: support_tpl

	,	title: _('Channel Ten - Support').translate()

	,	page_header: _('Support').translate()

	,	initialize: function (options)
		{
			var self = this;
		}

		// @method getContext @return FusionSupport.View.Context
	,	getContext: function()
		{
			return {
			};
		}
	});
});
