<div class="page-header custom-page-header resellers-header">
    <div class="container">
        <div class="middle-align">
            <div data-cms-area="resellers_header_content" data-cms-area-filters="path"></div>
        </div>
    </div>
</div>

<div class="container custom-page resellers">
    <div class="row introduction">
        <div data-cms-area="resellers_introduction" data-cms-area-filters="path"></div>
    </div>
    <div class="row reseller-logos">
        <div class="col-md-6 logo-container">
            <div data-cms-area="resellers_image_three" data-cms-area-filters="path"></div>
            <div data-cms-area="resellers_caption_three" data-cms-area-filters="path"></div>
        </div>
        <div class="col-md-6 logo-container">
            <div data-cms-area="resellers_image_four" data-cms-area-filters="path"></div>
            <div data-cms-area="resellers_caption_four" data-cms-area-filters="path"></div>
        </div>
    </div>
</div>

<!--<div class="shaded-background">
    <div class="container custom-page">
    </div>
</div>-->
